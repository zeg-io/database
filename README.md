# Template Database

A collection of common templates for Zetta applications.

## Installation
Install Code Forge

`npm i -g code-forge`

Install the remote templates

`forge load-remote <url>`
